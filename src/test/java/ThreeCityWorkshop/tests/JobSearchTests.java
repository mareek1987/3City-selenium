package ThreeCityWorkshop.tests;

import static org.junit.Assert.*;
import ThreeCityWorkshop.pages.PageObjectManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by BEST-Laptop on 05.07.2016.
 */
public class JobSearchTests {
    private static final String pageUrl = "http://www.trojmiasto.pl/";

    private WebDriver driver;
    private PageObjectManager manager;

    @Before
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        manager = new PageObjectManager(driver);
        driver.get(pageUrl);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

    }
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void jobSearchTest() {
        String jobType = "Tester";

        manager.getHomePage().clickOnJobOfferLink();
        manager.getSearchJobPage().typeJobName(jobType);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //assertEquals("Znaleźliśmy dla Ciebie",);
    }
}
