package ThreeCityWorkshop.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by BEST-Laptop on 05.07.2016.
 */
public class PageObjectManager {
    private final WebDriver driver;
    private HomePage homePage;
    private SearchJobPage searchJobPage;

    public PageObjectManager( WebDriver driver) {
        super();
        this.driver = driver;
    }
    public HomePage getHomePage() {
        if (homePage == null) {
            homePage = PageFactory.initElements(driver, HomePage.class);
        }
        return homePage;
    }
    public SearchJobPage getSearchJobPage(){
        if (searchJobPage==null){
            searchJobPage = PageFactory.initElements(driver,SearchJobPage.class);
        }
        return searchJobPage;
    }
}
