package ThreeCityWorkshop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by BEST-Laptop on 05.07.2016.
 */
public class HomePage {

    @FindBy(linkText = "Oferty pracy")
    private WebElement jobOfferElement;

    public void clickOnJobOfferLink() { jobOfferElement.click();}
}
