package ThreeCityWorkshop.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by BEST-Laptop on 05.07.2016.
 */
public class SearchJobPage {

    @FindBy(id = "ogl_search")
    private WebElement mainSearchTextField;
    @FindBy(css = "[type='submit'][value='Szukaj']")//(xpath = "/html/body/div[2]/div[3]/form/div[1]/div/ul[2]/li[2]/button")
    private WebElement searchButton;
    public void typeJobName( String jobType) {mainSearchTextField.sendKeys(jobType);}
}
